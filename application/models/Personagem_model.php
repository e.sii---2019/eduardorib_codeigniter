<?php

class Personagem_model extends CI_Model {

    protected $table = 'personagens';

    public function Todos($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table);

        return $query->result();
    }

    public function Create($nome, $descricao = null) {
        $this->db->query("INSERT INTO personagens (nome, descricao) VALUES ('".$nome."', '".$descricao."')");
    }

    public function Destroy($id) {
        $this->db->query("DELETE FROM personagens WHERE id = ".$id);
    }

    public function Show($id) {
        $query = $this->db->get_where('personagens', array('id' => $id));
        return $query->row_array();
    }

    public function size() {
        return $this->db->count_all($this->table);
    }
}