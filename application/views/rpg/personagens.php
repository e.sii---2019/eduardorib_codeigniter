<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RPG</title>
</head>
<body>
    <h1 style="text-align: center; margin-bottom: 3%">Personagens Cadastrados</h1>
    <div class="container">
        <div class="col-sm-12" style="text-align: center; margin-bottom: 3%">
            <form action="create" method="POST" enctype="multipart/form-data">
                <label for="nome">Nome:</label>
                <input type="text" name="nome">
                <label for="descricao">Descrição:</label>
                <input type="text" name="descricao">
                <input type="submit" class="btn btn-warning" value="Cadastrar">
            </form>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Descricao</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($personagens as $p) : ?>
                <tr>
                    <td><?= $p->id ?></td>
                    <td><?= $p->nome ?></td>
                    <td><?= $p->descricao ?></td>
                    <td>
                        <div class="row">
                            <form action="personagens/show" method="GET">
                                <input type="hidden" name="id" value="<?= $p->id ?>">
                                <input type="submit" class="btn btn-success" value="Visualizar">
                            </form>
                            &nbsp
                            <form action="personagens/destroy" method="GET">
                                <input type="hidden" name="id" value="<?= $p->id ?>">
                                <input type="submit" class="btn btn-danger" value="Excluir">
                            </form>
                        </div>
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div style="text-align: center">
        <p><?php echo $links; ?></p>
    </div>
</body>
</html>