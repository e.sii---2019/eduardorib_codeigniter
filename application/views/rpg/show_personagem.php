<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Visualização de Personagem</title>
    </head>
    <body>
        <h1 style="text-align: center; margin-bottom: 4%"> Ver Personagem </h1>
        <div style="text-align: center">
            <label for="nome">Nome:</label>
            <input type="text" name="nome" id="nome" value= <?= $personagem['nome'] ?>>
            <label for="descricao">Descrição</label>
            <input type="text" name="descricao" id="descricao" value= <?= $personagem['descricao'] ?>>
            <br/>
            <input type='button' value='Voltar' class="btn btn-success" onclick='history.go(-1)' />
        </div>
    </body>
</html>