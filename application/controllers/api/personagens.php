<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class personagens extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("personagens", ['id' => $id])->row_array();
        }else{
            $data = $this->db->get("personagens")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->input->post();
        $this->db->insert('personagens',$input);
     
        $this->response(['Personagem criado com sucesso.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('personagens', $input, array('id'=>$id));
     
        $this->response(['Personagem atualizado com sucesso.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('personagens', array('id'=>$id));
       
        $this->response(['Personagem deletado com sucesso.'], REST_Controller::HTTP_OK);
    }
    	
}