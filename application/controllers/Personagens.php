<?php

class Personagens extends CI_Controller {

    public function __Construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personagem_model');
        $this->load->library("pagination");
    }

    public function index(){
        $config = array();
        $config["base_url"] = base_url() . "personagens";
        $config["total_rows"] = $this->Personagem_model->size();
        $config["per_page"] = 3;
        $config["uri_segment"] = 2;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $data["links"] = $this->pagination->create_links();

        $data['personagens'] = $this->Personagem_model->Todos($config["per_page"], $page);

        $this->load->view('rpg/personagens', $data);

    }

    public function create() {
        $nome = $this->input->post('nome');
        $descricao = $this->input->post('descricao');
        $this->Personagem_model->Create($nome, $descricao);
        $this->index();
    }

    public function destroy() {
        $id = $this->input->get('id');
        $this->Personagem_model->Destroy($id);
        $this->index();
    }

    public function show() {
        $id = $this->input->get('id');
        $dado = $this->Personagem_model->Show($id);
        $this->load->view('rpg/show_personagem.php', array('personagem' => $dado));
    }
}