<?php

class Migration_Create_personagens extends CI_Migration {

    public function up() 
    {
        $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                ),
                'nome' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '120',
                ),
                'descricao' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '300',
                ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('personagens');
    }

    public function down()
    {
        $this->dbforge->drop_table('personagens');
    }

}