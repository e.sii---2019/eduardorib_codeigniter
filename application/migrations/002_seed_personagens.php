<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Seed_personagens extends CI_Migration {

    public function up()
    {
        foreach ($this->seedData as $seed ) {
            $this->db->query($seed);
        }
    }

    public function down()
    {
        foreach ($this->seedData as $seed) {
            $hash = substr($seed, 2, 32);
            $sql = 'DELETE FROM hash_table WHERE hash = \''.$hash.'\'';
            $this->db->query($sql);
        }
    }

    private $seedData = array(
        "INSERT INTO personagens (nome, descricao) VALUES ('John Snow', 'Não sabe de nada')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Tormund', 'Pedra Dura')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Legolas', 'Elfo Macho Alpha')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Justin Bieber', 'Elfo das Florestas')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Merlin', 'Bruxão')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Aldair', 'Vampiro')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Glubdor', 'Cavaleiro Audacioso')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Ragnar', 'Viking')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Stuardy', 'Mero Fazendeiro')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Frodo', 'Guardião do Anel')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Lindoia', 'Rainha Psicopata')",
        "INSERT INTO personagens (nome, descricao) VALUES ('Claudislei', 'Visitante local')",
    );
}