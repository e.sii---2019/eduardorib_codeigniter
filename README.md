# Projeto CodeIgniter

 Projeto desenvolvido na cadeira de Engenharia de Software II, Senac Pelotas. O intuito é aprender e desenvolver uma aplicação em um framework de escolha. Escolhi neste projeto o framework php CodeIgniter 3. 

 Tecnologias:
 
 *	PHP
 *	CodeIgniter 3
 *	Apache
 *	MySQL
 *	HTML
 *	CSS
 *	Bootstrap

 Licença:
 
*  MIT License
*  Link: https://gitlab.com/e.sii---2019/eduardorib_codeigniter/blob/master/license.txt

 Video de instalação e conexão com banco de dados: https://youtu.be/DFHJYRKsLiY

 Requisitos para rodar:
 
 1.  MySQL
 1.  Apache
 1.  Xampp (recomendado)

 Executando:

 1.	Faça o download e instalação do Xampp. (https://www.apachefriends.org/download.html)
 1.	Abra o Xampp, e no painel de controle clique em "start" no apache e mysql
 1.	Faça a criação de um banco de dados chamado: "rpgci"
 1.	Faça um clone ou download(.zip) deste projeto na pasta "htdocs" dentro do diretório Xampp, para que seja possível a localização do projeto por parte do Apache (Caso não esteja utilizando xampp, verifique como inicializar o servidor via comando php)
 1.	Abra o cmd e navegue até a pasta raiz do projeto, rode o comando "php index.php migrate"
 1.	A seguir, rode o comando "php -S localhost:7000"
 1.	Abra um navegador de sua escolha
 1.	Insira na barra de pesquisa:  "localhost:7000/personagens"
 1.	A tela principal estará pronta para uso








